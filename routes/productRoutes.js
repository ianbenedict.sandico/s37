const express = require('express');
const router = express.Router();
const productController = require('../controllers/productControllers');
const userController = require('../controllers/userControllers');
const auth = require('../auth')

//Retrieve all products (for reference)
router.get('/all', (req,res) => {
	productController.getAllProduct().then(result => res.send(result));
})


//Route for all active products (all users)
router.get('/', (req,res) =>{
	productController.getAllActive().then(result => res.send(result));
})

//Route for creating products (admin only)
router.post('/', auth.verify, (req,res) =>{
	// console.log(auth.decode(req.headers.authorization).isAdmin)
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	if(data.isAdmin){
		productController.addProduct(data).then(result => res.send(result));
	}else{
		res.send(false)
	}

	// console.log(data)
	
})

//Retrieve Single Product (all users)

router.get("/:Id", (req,res) => {
	productController.specificProduct(req.params).then(result => res.send(result));
})


//Update Product Information (admin only)
router.put("/:Id", auth.verify, (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
		productController.updateProduct(req.params, req.body).then(result => res.send(result));
	}else{
		res.send(false)
	}
})


//Archive (admin only)
router.put("/:Id/archive", auth.verify,(req,res)=>{
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
		productController.archieveProduct(req.params).then(result => res.send(result));
	}else{
		res.send(false)
	}
})


// //Route for add product //Ivan 
// router.post('/product', auth.verify,(req,res) =>{
// 	productController.newProduct(req.body).then(result => res.send(result))
// })



module.exports = router


// if(data.isAdmin){
// 	Controller.functionName(req.body).then(result => res.send(result))
// 	}else{
// 		res.send(false)
// 	}
