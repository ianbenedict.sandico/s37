const Product = require ('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth')
const User = require('../models/User');

//Retrieve all products (for reference)
module.exports.getAllProduct = () => {
	return Product.find({}).then( result =>{
		return result;
	})
}

//Retrieve all active products (all users)
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}


// Add Product (For Admin Only)

module.exports.addProduct = (data) => {
	
	let newProduct = new Product({
		name: data.product.name,
		description: data.product.description,
		price: data.product.price
	})
	return newProduct.save().then((course,error) =>{
		if(error){
			return false;
		}else{	
			return true;
		}
	})
	
}

//Retrieve Single Product (all users)

module.exports.specificProduct = (reqParams) => {
	// console.log(reqParams)
	return Product.findById(reqParams.Id).then(result =>{
		return result;
	})
}

//Update Product Information (admin only)

module.exports.updateProduct = (reqParams,reqBody) =>{
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Product.findByIdAndUpdate(reqParams.Id, updatedProduct).then((course,error) =>{
		if(error){
			return false;
		}else{
			return true;
		}
	})
}



//Archive (admin only)

module.exports.archieveProduct = (reqParams) => {
	let updatedProduct = {
		isActive: false
	}
	return Product.findByIdAndUpdate(reqParams.Id,updatedProduct).then((course,error) =>{
		if(error){
			return false;
		}else
			return true;
	})
}

// //Check if product already exists

// module.exports.checkProduct = (reqBody) => {

// 	 Product.find({name: reqBody.name}).then(result =>{
		
// 		if(result.length > 0){
// 			return true;
// 		}else{

// 			return false;
// 		}

// 	})
// }







// //Add Product //Ivan
// module.exports.newProduct = (reqBody) =>{
// 	return User.findById({_id: reqBody.userId}).then(user =>{
// 		if(user.isAdmin == true){
// 			 Product.find({name: reqBody.name}).then(result =>{
				
// 				if(result.length > 0){
// 					return true;
// 				}else{
// 					let newProduct = new Product({
// 						name: reqBody.name,
// 						description: reqBody.description,
// 						price: reqBody.price
// 					})
// 					return newProduct.save().then((user, error) => {
// 						if(error){
// 							return false;
// 						}else{
// 							return true;
// 						}
// 					})
// 				}

// 			})

// 		}else{
// 			console.log("Need to be an Admin")
// 		}
// 	})

// }

