const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	email:{
		type: String,
		required: [true, "email is required"]
	},
	password: {
		type: String,
		required: [true, "password is required"]
	},
	isAdmin: {
		type: Boolean,	
		default: false
	},
	checkout: [
	{
		productId:{
			type: String,
			required: [true, 'Product ID is required']
		},
		totalAmount: {
			type: Number,
			required: [true, "Total Amount is required"]
		},
		purchasedOn:{
			type: Date,
			default: new Date(),
		}

	}]
})	

module.exports = mongoose.model('User', userSchema);