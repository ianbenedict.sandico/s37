const express = require ('express');
const mongoose = require('mongoose');

const cors = require('cors');

const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')


const app = express();

mongoose.connect('mongodb+srv://admin:admin@zuitt-bootcamp.o2ycp.mongodb.net/Batch127_capstone2?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once('open',() => console.log('Now connected to MongoDb Atlas'));


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use('/users', userRoutes)
app.use('/product', productRoutes)

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
})

