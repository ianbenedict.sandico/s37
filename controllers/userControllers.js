const User = require ('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth')
const Product = require('../models/Product')


//Check if email exists

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result =>{
		
		if(result.length > 0){
			return true;
		}else{

			return false;
		}

	})
}

//User Registration
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password,10)
	})
	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{
		
		if(result == null){
			return false;
		}else{
			
			const isPasswordCorrect= bcrypt.compareSync(reqBody.password, result.password)
			
				if(isPasswordCorrect){
					
					return { access: auth.createAccessToken(result.toObject()) }
				}else{

				}	return false;
		}
	})
}

// Update admin status
module.exports.updateAdmin = (data) => {
	console.log(data)
	return User.findById(data.userId).then((user, error) =>{
		if(error){
			console.log(error)
			return false;
		}
		if(user.isAdmin){
			user.isAdmin = false
		}else{
			user.isAdmin = true;
		}
		
		return user.save().then((updatedAdmin, errAdmin) =>{
			if(errAdmin){
				console.log(errAdmin);
				return false;
			}else{
				return updatedAdmin;
			}
		})
	})
}

//Get Profile of user

module.exports.getProfile = (userId) => {
	return User.findById(userId).then(user => {
		return user
	})
}

module.exports.checkout = async(data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.checkout.push({
			productId: data.product.productId, 
			totalAmount: data.product.totalAmount
		})
	

		return user.save().then((user,error) =>{
			if(error){
				return false;
			}else{
				return true;
			}
		})
	})

	let isProductUpdated = await Product.findById(data.product.productId).then(product =>{
		
		product.orders.push({ userId: data.userId});

		return product.save().then((product,error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
	})

	if(isUserUpdated && isProductUpdated){
		return true;
	}else{
		return false;
	}
}

//Retrieve authenticated user's orders
module.exports.retrieveCheckout = (userId) => {
	return User.findById({_id:userId}).then(user => {
		console.log(user.checkout)
	return user.checkout
	}) 
}

//Retrieve all products (for reference)
module.exports.getAllOrders = (data) => {
	if(data.isAdmin){
		return User.find({}).then( result =>{

		const checkouts = result.map(data => {
			console.log(data.checkouts)
			let obj = {
				userId: data._id,
				checkouts: data.checkout,
			}
			console.log(obj)
			return obj
			})
		return checkouts
		})
	}
	
}	