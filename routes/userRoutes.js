const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers');
const auth = require('../auth')


//Route for user registration
router.post('/register', (req,res) =>{
	userController.checkEmailExists(req.body).then(resultEmail => {
		if(!resultEmail){
			userController.registerUser(req.body).then(result => res.send(result));
		}else{
			console.log("User already registered")
		}
	});
	
})

//Route for user authentication
router.post('/login', (req,res) =>{
	userController.loginUser(req.body).then(result => res.send(result));
})

router.put('/:id', auth.verify,(req,res) =>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: req.params.id
	}
	if(data.isAdmin){
		userController.updateAdmin(data).then(result => res.send(result));
	}else{
		res.send(false)
	}
})

//Routes to put a product in the checkout
router.post('/checkout', auth.verify, (req,res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		product: req.body
	}
	userController.checkout(data).then(result => res.send(result))
})

//Retrieve authenticated user's orders
router.get('/retrievecheckout', auth.verify, (req,res) =>{

	userController.retrieveCheckout(auth.decode(req.headers.authorization).id).then(result => res.send(result));
})


//Retrieve all products (for reference)
router.get('/all', auth.verify ,(req,res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id
	}
	userController.getAllOrders(data).then(result => res.send(result));
})

module.exports = router